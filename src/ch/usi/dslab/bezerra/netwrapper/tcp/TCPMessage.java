/*

 Netwrapper - A library for easy networking in Java
 Copyright (C) 2014, University of Lugano
 
 This file is part of Netwrapper.
 
 Netwrapper is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 
*/

package ch.usi.dslab.bezerra.netwrapper.tcp;

import java.nio.ByteBuffer;

import ch.usi.dslab.bezerra.netwrapper.Message;

public class TCPMessage {
   TCPConnection connection;
   Message       contents;
   ByteBuffer    serializedContents;
   
   public TCPMessage(TCPConnection connection, Message contents) {
      this.connection = connection;
      this.contents   = contents;
   }
   
   public TCPMessage(TCPConnection connection, ByteBuffer serializedContents) {
      this.connection = connection;
      this.serializedContents = serializedContents;
   }
   
   public TCPConnection getConnection() {
      return connection;
   }
   
   public Message getContents() {
      return contents;
   }
}
