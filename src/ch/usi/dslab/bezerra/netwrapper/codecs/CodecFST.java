package ch.usi.dslab.bezerra.netwrapper.codecs;

import ch.usi.dslab.bezerra.netwrapper.Message;
import com.esotericsoftware.kryo.io.ByteBufferInputStream;
import de.ruedigermoeller.serialization.FSTConfiguration;
import de.ruedigermoeller.serialization.FSTObjectInput;
import de.ruedigermoeller.serialization.FSTObjectOutput;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;

/**
 * Created by longle on 11.04.17.
 */
public class CodecFST implements Codec {

    static FSTConfiguration conf = FSTConfiguration.createDefaultConfiguration();

    static {
        getInstance().registerClass(Message.class);
        getInstance().registerClass(ArrayList.class);
    }


    public static FSTConfiguration getInstance() {
        return conf;

    }

    public CodecFST() {

    }

    @Override
    public byte[] getBytes(Object obj) {
        FSTObjectOutput out = getInstance().getObjectOutput();
        try {
            out.writeObject(obj, Object.class);
            out.flush();
            byte[] data2 = out.getCopyOfWrittenBuffer();
            return data2;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Object createObjectFromBytes(byte[] bytes) {
        FSTObjectInput in;
        Object obj = null;
        try {
            in = getInstance().getObjectInput(bytes);
            obj = in.readObject();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return obj;
    }

    @Override
    public ByteBuffer getByteBufferWithLengthHeader(Object obj) {
        byte[] objectArray = getBytes(obj);
        int byteBufferLength = 4 + objectArray.length;
        ByteBuffer extByteBuffer = ByteBuffer.allocate(byteBufferLength);
        extByteBuffer.putInt(byteBufferLength - 4); // length in first header doesn't
        // include that header's length
        extByteBuffer.put(objectArray);
        extByteBuffer.flip();
        return extByteBuffer;
    }

    @Override
    public Object createObjectFromByteBufferWithLengthHeader(ByteBuffer buffer) {
        // skip the buffer length parameter
        buffer.position(buffer.position() + 4);
        FSTObjectInput in = getInstance().getObjectInput(new ByteBufferInputStream(buffer));
        Message msg = null;
        try {
            msg = (Message) in.readObject();
            msg.rewind();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return msg;
    }

    @Override
    public Object deepDuplicate(Object o) {
        return (new CodecUncompressedKryo()).deepDuplicate(o);
    }

    @Override
    public String getString(Object m) {
        return null;
    }

    @Override
    public Object createObjectFromString(String string) {
        return null;
    }
}
