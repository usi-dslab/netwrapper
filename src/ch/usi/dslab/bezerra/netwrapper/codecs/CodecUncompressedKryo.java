/*

 Netwrapper - A library for easy networking in Java
 Copyright (C) 2014, University of Lugano
 
 This file is part of Netwrapper.
 
 Netwrapper is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 
*/

package ch.usi.dslab.bezerra.netwrapper.codecs;

import ch.usi.dslab.bezerra.netwrapper.Message;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.ByteBufferInputStream;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import com.esotericsoftware.kryo.util.FastestStreamFactory;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

@SuppressWarnings("rawtypes")
public class CodecUncompressedKryo implements Codec {

    private static Map<Integer, Class> classesToRegister = new ConcurrentHashMap<Integer, Class>();

    public static void registerClassInSerializationIndex(Class c, int id) {
        classesToRegister.put(id, c);
    }

    private static ThreadLocal<Kryo> threadKryo = new ThreadLocal<Kryo>() {
        @Override
        protected Kryo initialValue() {
            final FastestStreamFactory streamFactory = new FastestStreamFactory();

            Kryo kryo = new Kryo();
            kryo.setStreamFactory(streamFactory);

            kryo.register(Message.class);
            kryo.register(ArrayList.class);
            kryo.setReferences(false);
            for (Entry<Integer, Class> pair : classesToRegister.entrySet())
                kryo.register(pair.getValue(), pair.getKey());
            return kryo;
        }
    };

    private static Kryo getCurrentThreadKryo() {
        Kryo kryo = threadKryo.get();
        return kryo;
    }

    @Override
    public byte[] getBytes(Object m) {
        Kryo kryo = getCurrentThreadKryo();
        byte[] bytes = null;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        Output out = new Output(bos);
        try {
            kryo.writeClassAndObject(out, m);
            out.flush();
            bytes = bos.toByteArray();
            bos.close();
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                bos.close();
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return bytes;
    }

    @Override
    public ByteBuffer getByteBufferWithLengthHeader(Object obj) {
        byte[] objectArray = getBytes(obj);
        int byteBufferLength = 4 + objectArray.length;
        ByteBuffer extByteBuffer = ByteBuffer.allocate(byteBufferLength);
        extByteBuffer.putInt(byteBufferLength - 4); // length in first header doesn't
        // include that header's length
        extByteBuffer.put(objectArray);
        extByteBuffer.flip();
        return extByteBuffer;
    }

    @Override
    public Object createObjectFromBytes(byte[] bytes) {
        Kryo kryo = getCurrentThreadKryo();
        Input in = new Input(bytes);
        Object obj = kryo.readClassAndObject(in);
        in.close();
        return obj;
    }

    @Override
    public Object createObjectFromByteBufferWithLengthHeader(ByteBuffer buffer) {
        Kryo kryo = getCurrentThreadKryo();

        // skip the buffer length parameter
        buffer.position(buffer.position() + 4);

        Input in = new Input(new ByteBufferInputStream(buffer));
        Message msg = (Message) kryo.readClassAndObject(in);
        in.close();
        msg.rewind();
        return msg;
    }

    @Override
    public Object deepDuplicate(Object o) {
        Kryo kryo = getCurrentThreadKryo();
        return kryo.copy(o);
    }


    public static String byteArrayToString(byte[] dataBytes) {
        return Base64.getEncoder().encodeToString(dataBytes);
    }

    public static byte[] stringToByteArray(String dataStr) {
        return Base64.getDecoder().decode(dataStr);
    }


    @Override
    public String getString(Object m) {
        try {
            ByteArrayOutputStream bo = new ByteArrayOutputStream();
            Output o = new Output(bo);
            Kryo kryo = getCurrentThreadKryo();
            kryo.writeClassAndObject(o, m);
            o.close();
            bo.close();
            return byteArrayToString(bo.toByteArray());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Object createObjectFromString(String string) {
        byte[] data = stringToByteArray(string);
        Kryo kryo = getCurrentThreadKryo();
        try {
            try (ByteArrayInputStream bais = new ByteArrayInputStream(data);
                 Input i = new Input(bais)) {
                Object obj;
                obj = kryo.readClassAndObject(i);
                return obj;
            }
        } catch (IOException e) {
            //ByteArrayInputStream Exception
            e.printStackTrace();
        }
        return null;
    }
}
