package ch.usi.dslab.bezerra.netwrapper.examples;

import ch.usi.dslab.bezerra.netwrapper.Message;
import ch.usi.dslab.bezerra.netwrapper.tcp.TCPMessage;
import ch.usi.dslab.bezerra.netwrapper.tcp.TCPReceiver;
import ch.usi.dslab.bezerra.netwrapper.tcp.TCPSender;

public class SimpleTcpServer {
   
   public static void main(String[] args) {
      TCPReceiver receiver = new TCPReceiver(50000);
      TCPSender   sender   = new TCPSender  ();
      
      while(!Thread.interrupted()) {
         TCPMessage msg = receiver.receive();
         String recvdMessage = (String) msg.getContents().getItem(0);
         System.out.println("Received message: " + recvdMessage);
         Message ack = new Message(new String("Ack for message \"" + recvdMessage + "\""));
         sender.send(ack, msg.getConnection());
      }
   }

}
