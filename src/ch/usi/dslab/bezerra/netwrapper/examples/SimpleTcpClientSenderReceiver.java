package ch.usi.dslab.bezerra.netwrapper.examples;

import java.io.IOException;

import ch.usi.dslab.bezerra.netwrapper.Message;
import ch.usi.dslab.bezerra.netwrapper.tcp.TCPConnection;
import ch.usi.dslab.bezerra.netwrapper.tcp.TCPMessage;
import ch.usi.dslab.bezerra.netwrapper.tcp.TCPReceiver;
import ch.usi.dslab.bezerra.netwrapper.tcp.TCPSender;

public class SimpleTcpClientSenderReceiver {

   public static void main(String[] args) throws InterruptedException, IOException {
      TCPSender sender = new TCPSender();
      
      Message message = new Message(new String("first message from client"));
      TCPConnection serverConnection = new TCPConnection("localhost", 50000);

      sender.send(message, serverConnection);

      TCPReceiver receiver = new TCPReceiver();
      receiver.addConnection(serverConnection);

      TCPMessage msg = receiver.receive();
      String reply = (String) msg.getContents().getItem(0);
      System.out.println("Reply received: " + reply);

      Message message2 = new Message(new String("second message from client"));

      sender.send(message2, serverConnection);

      TCPMessage msg2 = receiver.receive();
      String reply2 = (String) (msg2.getContents().getItem(0));
      System.out.println("Reply received: " + reply2);
      
      sender.stop();
      receiver.stop();
   }


}
